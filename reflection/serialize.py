import inspect


################################################
###   Serialization to JSON

TAB = '\t'
PRIMATIVES = (
    type(None), bool, float, int, str
)


def to_json(obj, level=0, final_comma=False):
    '''Serializes the given object to JSON, printing to the console as it goes.'''
   
    if level == 0:
        print('{')

    level += 1
    count = 1
    total = len(obj.__dict__.items())
    
    for attr, value in obj.__dict__.items():
        
        
        trailing_comma = ''
        add_comma = False
        if count < total:
            trailing_comma = ','
            add_comma = True
        if isinstance(value, PRIMATIVES):
            
            string_value = to_string(value)
            print(level * TAB + '"' + attr + '": ' + string_value + trailing_comma)
        
        else:
            print(level * TAB + '"' + attr + '": {')
            to_json(value, level, add_comma)
        count += 1
    trailing_comma = ''
    if level != 1 and final_comma:
        trailing_comma = ','

    print((level - 1) * TAB + '}' + trailing_comma)

def to_string(value):
    if isinstance(value, type(None)):
        return 'null'
    elif isinstance(value, bool):
        if value:
            return 'true'
        else:
            return 'false'
    elif isinstance(value, str):
        
        string_array = value.split('\\')
        string_array = '\\\\'.join(string_array)

        string_array = string_array.split('"')
        string_array = '\\"'.join(string_array)
        return '\"' + string_array + '\"'
    
    else:
        return str(value)


