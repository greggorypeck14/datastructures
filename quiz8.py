def squareRoot(number):
    root = 0
    while root * root <= number:
        root += 1

    return root

print(squareRoot(16))