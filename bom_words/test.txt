
1 Nephi

Chapter 1

Nephi begins the record of his people—Lehi sees in vision a pillar of fire and reads from a book of prophecy—He praises God, foretells the coming of the Messiah, and prophesies the destruction of Jerusalem—He is persecuted by the Jews. About 600 B.C.



1 I, Nephi, having been born of goodly parents, therefore I was taught somewhat in all the learning of my father; and having seen many afflictions in the course of my days, nevertheless, having been highly favored of the Lord in all my days; yea, having had a great knowledge of the goodness and the mysteries of God, therefore I make a record of my proceedings in my days.

2 Yea, I make a record in the language of my father, which consists of the learning of the Jews and the language of the Egyptians.

3 And I know that the record which I make is true; and I make it with mine own hand; and I make it according to my knowledge.
