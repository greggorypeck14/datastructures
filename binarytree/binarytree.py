#!/usr/bin/env python3
from collections import deque

class BinaryTree(object):
    '''
    A binary tree.
    '''
    def __init__(self):
        self.root = None


    def __repr__(self):
        return repr(self.root)


    def set(self, key, value):
        '''
        Adds the given key=value pair to the tree.
        '''
        if self.root:
            endNode = False
            currentNode = self.root
            while endNode is False:
                # go left
                if key <= currentNode.key:
                    # change and loop
                    if currentNode.left:
                        currentNode = currentNode.left
                        
                    # Add to left
                    else:
                        endNode = True
                        newnode = Node(currentNode, key, value)
                        currentNode.left = newnode
                # go right
                else:
                     # change and loop
                    if currentNode.right:
                        currentNode = currentNode.right
                        
                    # Add to right
                    else:
                        endNode = True
                        newnode = Node(currentNode, key, value)
                        currentNode.right = newnode

        else:
            self.root = Node(None, key, value)


    def get(self, key):
        '''
        Retrieves the value under the given key.
        Returns None if the key does not exist.
        '''
        value = ''
        node = self._find(key)
        if node:
            value = node.value
        return value


    def remove(self, key):
        '''
        Removes the given key from the tree.
        Returns silently if the key does not exist.
        '''
        nodeToRemove = self._find(key)
        if nodeToRemove:
            if nodeToRemove.left or nodeToRemove.right:
                # move up end nodes up 
                if nodeToRemove.left and nodeToRemove.right is None:
                    self._replace_node(nodeToRemove, nodeToRemove.left)
                    
                elif nodeToRemove.right and nodeToRemove.left is None:
                    self._replace_node(nodeToRemove, nodeToRemove.right)
                
                # both leaves are filled...  go left and find max
                else:
                    endNode = False
                    maxNode = nodeToRemove.left
                    while endNode is False:
                        if maxNode.right:
                            maxNode = maxNode.right
                        else:
                            endNode = True
                            # print(maxNode.parent.key)
                            maxNode.parent.right = None
                            maxNode.left = nodeToRemove.left
                            maxNode.right = nodeToRemove.right

                    
                    
                    self._replace_node(nodeToRemove, maxNode)

                    # print(maxNode.key, maxNode.parent, maxNode.left.key, maxNode.right.key)

                    
            # just remove
            else:
                self._replace_node(nodeToRemove, None)



    def walk_dfs_inorder(self, currentNode=None):
        '''
        An iterator that walks the tree in DFS fashion.
        Yields (key, value) for each node in the tree.
        '''
        #TODO
        # "yield (key, value)" for current node
        # "yield from walk_*()" when recursing
        if currentNode is None:
            currentNode = self.root
        if currentNode.left:
            yield from self.walk_dfs_inorder(currentNode.left)
        yield (currentNode.key, currentNode.value)
        if currentNode.right:
            yield from self.walk_dfs_inorder(currentNode.right)
        

    def walk_dfs_preorder(self, node=None, level=0):
        '''
        An iterator that walks the tree in preorder DFS fashion.
        Yields (key, value) for each node in the tree.
        '''
        #TODO
        # "yield (key, value)" for current node
        # "yield from walk_*()" when recursing
        if node is None:
            node = self.root
        yield (node.key, node.value)
        if node.left:
            yield from self.walk_dfs_preorder(node.left)
        if node.right:
            yield from self.walk_dfs_preorder(node.right)
     


    def walk_dfs_postorder(self, node=None, level=0):
        '''
        An iterator that walks the tree in inorder DFS fashion.
        Yields (key, value) for each node in the tree.
        '''
        #TODO
        # "yield (key, value)" for current node
        # "yield from walk_*()" when recursing
        if node is None:
            node = self.root
        if node.left:
            yield from self.walk_dfs_postorder(node.left)
        if node.right:
            yield from self.walk_dfs_postorder(node.right)
        yield (node.key, node.value)


    def walk_bfs(self):
        '''
        An iterator that walks the tree in BFS fashion.
        Yields (key, value) for each node in the tree.
        '''
        #TODO
        # "yield (key, value)" for current node
        
        


        nodeList = [self.root]
        
        while len(nodeList) > 0:
            currentNode = nodeList.pop(0)
            yield (currentNode.key, currentNode.value)
            if currentNode.left:
                nodeList.append(currentNode.left)
            if currentNode.right:
                nodeList.append(currentNode.right)
        



    ##################################################
    ###   Helper methods


    def _replace_node(self, oldnode, newnode):
        '''
        Internal method to remove a node from its parent
        '''
        #TODO: feel free to use or remove this method
        if oldnode.parent.left == oldnode:
            oldnode.parent.left = newnode
        elif oldnode.parent.right == oldnode:
            oldnode.parent.right = newnode
        else:
            print('issue in the remove - no match for left / right end node')
        if newnode:
                newnode.parent = oldnode.parent        

    def _find(self, key):
        '''
        Internal method to find a node by key.
        Returns (parent, node).
        '''
        #TODO: feel free to use or remove this method
        returnNode = None
        if self.root:
            endNode = False
            currentNode = self.root
            while endNode is False:
                if key == currentNode.key:
                    returnNode = currentNode
                    endNode = True
                else:
                    # go left
                    if key < currentNode.key:
                        # change and loop
                        if currentNode.left:
                            currentNode = currentNode.left
                            
                        # End of tree
                        else:
                            endNode = True
                            
                    # go right
                    else:
                        # change and loop
                        if currentNode.right:
                            currentNode = currentNode.right
                            
                        # End of tree
                        else:
                            endNode = True
                            
        return returnNode
        # else:
        #     raise ValueError("No nodes have been created in tree")



class Node(object):
    '''
    A node in a binary tree.
    '''
    def __init__(self, parent, key, value):
        '''Creates a linked list.'''
        self.key = key
        self.value = value
        self.parent = parent
        self.left = None
        self.right = None

    def __repr__(self):
        result = []
        def recurse(node, prefix1, side, prefix2):
            if node is None:
                return
            result.append(prefix1 + node.key + side)
            if node.right is not None:
                recurse(node.left, prefix2 + '\u251c\u2500\u2500 ', ' \u2c96', prefix2 + '\u2502   ')
            else:
                recurse(node.left, prefix2 + '\u2514\u2500\u2500 ', ' \u2c96', prefix2 + '    ')
            recurse(node.right, prefix2 + '\u2514\u2500\u2500 ', ' \u1fe5', prefix2 + '    ')
        recurse(self, '', '', '')
        return '\n'.join(result)
