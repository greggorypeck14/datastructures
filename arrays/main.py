#!/usr/bin/env python3
import csv
from array_api import Array

def main():
    try:
                
        # output_file = open("output.txt", "w")

        array = None
        output_line = 0

        with open('data.csv') as csvDataFile:
            csvReader = csv.reader(csvDataFile)
            for row in csvReader:
                try:
                    print(str(output_line) + ':' + row[0] + "," + row[1] + "," + row[2])
                    # output_file.write(str(output_line) + ':' + row[0] + "," + row[1] + "," + row[2] +"\n")
                    
                    method = row[0]
                    param1 = row[1]
                    param2 = row[2]
                    output = None

                    if method == 'CREATE':
                        array = Array()
                    if array:
                        if method == 'DEBUG':
                            array.debug_print()
                        elif method == 'ADD':
                            array.add(param1)
                        elif method == 'INSERT':
                            array.insert(param1, param2)
                        elif method == 'SET':
                            array.set(param1, param2)
                        elif method == 'GET':
                            array.get(param1)
                        elif method == 'DELETE':
                            array.delete(param1)
                        elif method == 'SWAP':
                            array.swap(param1, param2)
                except Exception as e:
                    return(str(e))
                    # output_file.write(str(e) +"\n")

                # print(method, param1, param2)
                output_line += 1
        # output_file.close() 


    except Exception as e:
        return(e)


# if __name__ == '__main__':
#   main()

main()
