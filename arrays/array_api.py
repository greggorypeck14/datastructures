#!/usr/bin/env python3

import math

class Array(object):
    '''
    An array implementation that holds arbitrary objects.
    '''

    def __init__(self, initial_size=10, chunk_size=5):
        '''Creates an array with an intial size.'''
        self.data = alloc(initial_size)
        self.size = 0


    def debug_print(self):
        '''Prints a representation of the entire allocated space, including unused spots.'''
        print('{} of {} >>> {}'.format(self.size, len(self.data), ', '.join([ str(item) for item in self.data ])))
        return '{} of {} >>> {}'.format(self.size, len(self.data), ', '.join([ str(item) for item in self.data ])) 

    def _check_bounds(self, index):
        '''Ensures the index is within the bounds of the array: 0 <= index <= size.'''
        if index < 0 or index >= self.size:
            # print ('Error: {} is not within the bounds of the current array.'.format(index))
            # return 'Error: {} is not within the bounds of the current array.'.format(index)
            return False
        return True

    def _check_increase(self):
        '''
        Checks whether the array is full and needs to increase by chunk size
        in preparation for adding an item to the array.
        '''
        if len(self.data) <= self.size:
            self.data = self.data + alloc(5)


    def _check_decrease(self):
        '''
        Checks whether the array has too many empty spots and can be decreased by chunk size.
        If a decrease is warranted, it should be done by allocating a new array and copying the
        data into it (don't allocate multiple arrays if multiple chunks need decreasing).
        '''
        if self.size <= 10:
            if len(self.data) > 10:
                self.data = memcpy([], self.data, 10)
        else:
            current_chunks = (len(self.data) - 10) / 5
            needed_chunks = math.ceil((self.size - 10) / 5)
            if current_chunks != needed_chunks:
                self.data = memcpy([], self.data, (needed_chunks * 5) + 10)


    def add(self, item):
        '''Adds an item to the end of the array, allocating a larger array if necessary.'''
        self._check_increase()
        self.data[self.size] = item
        self.size += 1
        self._check_decrease()

    def insert(self, index, item):
        '''Inserts an item at the given index, shifting remaining items right and allocating a larger array if necessary.'''
        index = self.convertIndex(index)
        try:
            if self._check_bounds(index):
                self._check_increase()
                original = self.data
                first_half = self.data[:index]
                second_half = self.data[index:]

                self.data = first_half + [item] + second_half
                       
                self.size += 1
                self._check_decrease()

            else:
                raise ValueError
        except ValueError:
            print('Error: {} is not within the bounds of the current array.'.format(index))
            return 'Error: {} is not within the bounds of the current array.'.format(index)


        



    def set(self, index, item):
        '''Sets the given item at the given index.  Throws an exception if the index is not within the bounds of the array.'''
        index = self.convertIndex(index)
        
        try:
            if self._check_bounds(index):
                self.data[index] = item

            else:
                raise ValueError
        except ValueError:
            print('Error: {} is not within the bounds of the current array.'.format(index))
            return 'Error: {} is not within the bounds of the current array.'.format(index)


    def get(self, index):
        '''Retrieves the item at the given index.  Throws an exception if the index is not within the bounds of the array.'''
        index = self.convertIndex(index)
        try:
            if self._check_bounds(index):
                print(self.data[index])
                return self.data[index]
            else:
                raise ValueError
        except ValueError:
            print('Error: {} is not within the bounds of the current array.'.format(index))
            return 'Error: {} is not within the bounds of the current array.'.format(index)

        

    def delete(self, index):
        '''Deletes the item at the given index, decreasing the allocated memory if needed.  Throws an exception if the index is not within the bounds of the array.'''
        index = self.convertIndex(index)
        try:
            if self._check_bounds(index):
                first_half = self.data[:index]
                second_half = self.data[index+1:]
                self.data = first_half + second_half + alloc(1)
                self.size -= 1
                self._check_decrease()
            else:
                raise ValueError
        except ValueError:
            print('Error: {} is not within the bounds of the current array.'.format(index))
            return 'Error: {} is not within the bounds of the current array.'.format(index)



    def swap(self, index1, index2):
        '''Swaps the values at the given indices.'''
        index1 = self.convertIndex(index1)
        index2 = self.convertIndex(index2)
        

        try:
            if not self._check_bounds(index1):
                raise ValueError(index1)    
            elif not self._check_bounds(index2):
                raise ValueError(index2)
            else:
                value1 = self.get(index1)
                value2 = self.get(index2)
                self.set(index1, value2)
                self.set(index2, value1)
            
        except ValueError as index:
            print('Error: {} is not within the bounds of the current array.'.format(index))
            return 'Error: {} is not within the bounds of the current array.'.format(index)


    def convertIndex(self, index):
        try:
            index = int(index)
            return index
        except Exception as e:
            raise ValueError('Error: ' + str(index) + ' is not a valid index integer.')



###################################################
###   Utilities

def alloc(size):
    '''
    Allocates array space in memory. This is similar to C's alloc function.
    '''
    data = []
    for i in range(size):
        data.append(None)
    return data


def memcpy(dest, source, size):
    '''
    Copies items from one array to another.  This is similar to C's memcpy function.
    '''
    for i in range(size):
        dest.append(source[i])
    return dest

