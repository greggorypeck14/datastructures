def mergedLists(queue1, queue2):
    mergedList = []

    while len(queue1) > 0 or len(queue2) > 0:
        if len(queue1) > 0 and len(queue2) > 0:
            if queue1[0] < queue2[0]:
                mergedList.append(queue1.pop(0))
                
            else:
                mergedList.append(queue2.pop(0))
                
        else:
            if len(queue1) > 0:
                mergedList.append(queue1.pop(0))
            elif len(queue2) > 0:
                mergedList.append(queue2.pop(0))             


    return mergedList


results = mergedLists([1, 4, 6, 9], [2, 3, 4, 5, 6, 8])
print(results)