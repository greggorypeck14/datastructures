def staircase(level):
    spaces = level
    stair = 1
    while spaces > 0:
        spaces = level - 1
        output = ''
        for i in range(spaces):
            output = output + ' '
        for i in range(stair):
            output = output + '#'
        print(output)
        stair += 1
        level -= 1
        

staircase(4)
