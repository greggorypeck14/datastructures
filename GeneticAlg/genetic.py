import csv
import time
import math
# import numpy
import random
# import decimal



class GeneticSolution(object):
    def __init__(self):
        self.class_name = ''
        self.student_count = 0
        self.class_length = 0
        self.days_preferred = {
            'monday': False,
            'tuesday': False,
            'wednesday': False,
            'thursday': False,
            'friday': False,
        }
        self.section_number = 1
        self.preferred_time = ''
        self.preferred_room_type = ''
        self.assigned_room_number = ''
        self.assigned_room_size = 0
        self.assigned_room_type = ''
        self.assigned_room_time = ''
        self.fitnessScore = 0
        self.elite = False
        self.crossover = False
        self.mutation = False



    def getFitness(self):
        #### fitness value 

        # IndvFitness = CapacityValue + PrefTime + PrefRoomType

        # CapacityValue is assigned:
        #     50 if capacity of room minus students in course is 0-5.
        #     40 if capacity of room minus students in course is 6-10.
        #     30 if capacity of room minus students in course is 11-15.
        #     20 if capacity of room minus students in course is 16-20.
        #     10 if capacity of room minus students in course is 21-25.
        #     0 if capacity of room minus students in course is 26+.
        #     The number of students can never be greater than the capacity of the room (that would be an invalid assignment).

        # PrefTime is assigned:
        #     25 if the time slot is within the preferred time of the course (morning or afternoon).
        #     15 if the time slot crosses over the noon hour (thus partially matching the preferred time).
        #     0 if the time slot is not within the preferred time of the course.

        # PrefRoomType is assigned:
        #     25 if the preferred type of the room is met.
        #     0 if the preferred type of the room is not met.

        try:
            score = 0
            extraSeats = self.assigned_room_size - self.student_count
            if extraSeats <= 5:
                score += 50
            elif extraSeats <= 10:
                score += 40
            elif extraSeats <= 15:
                score += 30
            elif extraSeats <= 20:

                score += 20
            elif extraSeats <= 25:
                score += 10
            
            if self.preferred_time == 'Morning':
                if self.assigned_room_time < 12 and (self.assigned_room_time + self.class_length) <= 12:
                    score += 25
                elif self.assigned_room_time < 12 and (self.assigned_room_time + self.class_length) > 12:
                    score += 15

            elif self.preferred_time == 'Afternoon':
                if self.assigned_room_time >= 12:
                    score += 25
                elif self.assigned_room_time < 12 and (self.assigned_room_time + self.class_length) > 12:
                    score += 15
            
            if self.preferred_room_type != 'None':
                if self.preferred_room_type == self.assigned_room_type:
                    score += 25
            else:
                # no penalty for no room preference
                score += 25
            self.fitnessScore = score
        except Exception as e:
            print(e)
            self.fitnessScore = 0
        


    def crossover(self, solution2):
        print()


    def mutate(self):
        print()

class ClassRoom(object):
    def __init__(self):
        self.room = ''
        self.capacity = 0
        self.type = ''
        self.assigned_time = {
            'monday': [],
            'tuesday': [],
            'wednesday': [],
            'thursday': [],
            'friday': [],
        }
        self.freeTimeBlocks = 0
        self.freeRoom = 0

    def getFreeSlot(self, day, classTime, preferred):
        dayassignments = self.assigned_time[day]
        # classTime = classTime.split('.')
        # hours = 0
        # minutes = 0
        # hours = classTime[0]
        # if len(classTime) > 0:
        #     minutes = 60 * (float(classTime[1] / 100))
        timeneeded = float(classTime) + .25
        # print(dayassignments)
        if dayassignments:
            # need to parse through time and find a slot -- get slots and try to find preferred
            available_slots = []
            starttime = 8
            class_endtime = 8
            for slot in dayassignments:
                
                timebetween = slot['starttime'] - starttime
                
                if timebetween > timeneeded:
                    endtime, nextstart = self.calculateEndTime(starttime, classTime)
                    available_slots.append({
                        'starttime': starttime,
                        'endtime': endtime,
                        'nextstart': nextstart
                    })
                
                starttime = slot['nextstart']
                
            ## check tail end of day
            if starttime < 17:
                if starttime + timeneeded <= 17:
                    endtime, nextstart = self.calculateEndTime(starttime, classTime)
                    available_slots.append({
                        'starttime': starttime,
                        'endtime': endtime,
                        'nextstart': nextstart
                    })
            returnslot = None
            ### if afternoon pref go through items normally and find latest, 
            # if morning go in reverse order and find earliest
            if preferred == 'Afternoon':
                for slot in available_slots:
                    returnslot = slot
            else:
                for index in range(len(available_slots)-1, -1, -1):
                    returnslot = available_slots[index]
            
            # print(returnslot)
            return returnslot
        
        else:
            # print('whole day open')
            # whole day is open, choose a slot
            if preferred == 'Morning':
                starttime = 8
            else:
                starttime = 12
            endtime, nextstart = self.calculateEndTime(starttime, classTime)
            return {
                'starttime': starttime,
                'endtime': endtime,
                'nextstart': nextstart
            }

    def checkIfOpenSlot(self, day, starttime, nextstart):
        dayassignments = self.assigned_time[day]
        if dayassignments:
            # need to parse through time and ensure no clashes
            openSlot = True
            for slot in dayassignments:
                if slot['starttime'] < nextstart and slot['nextstart'] > nextstart:
                    openSlot = False
                elif slot['starttime'] < starttime and slot['nextstart'] > starttime:
                    openSlot = False
            return openSlot
        
        else:
            return True

    def calculateOpenSlots(self):
        days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday']
        openSlots = 0
        freeRoom = True
        for day in days:
            if self.assigned_time[day]:
                freeRoom = False
                starttime = 8
                for assignment in self.assigned_time[day]:
                    freetime = assignment['starttime'] - starttime
                    if (freetime / 2) > openSlots:
                        openSlots = (freetime / 2)
                    starttime = assignment['nextstart']
                ## end of day check
                if starttime < 17:
                    freetime = 17 - starttime
                    if (freetime / 2) > openSlots:
                        openSlots = (freetime / 2)
            else:
                openSlots = 18
        self.freeTimeBlocks = score
        if freeRoom:
            self.freeRoom = 1    
        
    
    def calculateEndTime(self, starttime, classTime):
        endtime = starttime + classTime
        nextstart = endtime + .25
        if nextstart.is_integer() is False:
            frac, whole = math.modf(nextstart)
            if frac <= .25:
                ### if class ends at the 15 min mark or less --
                # next class can start at next half hour
                nextstart = whole + .5
            else:
                ## if not, next class starts at the next complete hour
                nextstart = whole + 1
        return endtime, nextstart

    def resetData(self):

def loadData():
    classes = []
    rooms = []
    with open('classes.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            try:
                # Course,Monday,Tuesday,Wednesday,Thursday,Friday,
                # Sections,Hours,Preferred Time,Preferred Room Type,
                # Students Per Section

                courseName = row[0]
                if courseName != 'Course':
                    Monday = row[1]
                    Tuesday = row[2]
                    Wednesday = row[3]
                    Thursday = row[4]
                    Friday = row[5]
                    Sections = int(row[6])
                    Hours = row[7]
                    PreferredTime = row[8]
                    PreferredRoomType = row[9]
                    StudentsPerSection = int(row[10])

                    # print(courseName)
                
                    if Sections > 1:
                        for sectionNumber in range(1, int(Sections) + 1):
                            class_object = GeneticSolution()
                            class_object.class_name = courseName
                            class_object.student_count = StudentsPerSection
                            class_object.class_length = float(Hours)
                            if Monday == 'x':
                                class_object.days_preferred['monday'] = True
                            if Tuesday == 'x':
                                class_object.days_preferred['tuesday'] = True
                            if Wednesday == 'x':
                                class_object.days_preferred['wednesday'] = True
                            if Thursday == 'x':
                                class_object.days_preferred['thursday'] = True
                            if Friday == 'x':
                                class_object.days_preferred['friday'] = True  
                            class_object.section_number = sectionNumber
                            class_object.preferred_time = PreferredTime
                            class_object.preferred_room_type = PreferredRoomType

                            classes.append(class_object)
                    else:
                        class_object = GeneticSolution()
                        class_object.class_name = courseName
                        class_object.student_count = StudentsPerSection
                        class_object.class_length = float(Hours)
                        if Monday == 'x':
                            class_object.days_preferred['monday'] = True
                        if Tuesday == 'x':
                            class_object.days_preferred['tuesday'] = True
                        if Wednesday == 'x':
                            class_object.days_preferred['wednesday'] = True
                        if Thursday == 'x':
                            class_object.days_preferred['thursday'] = True
                        if Friday == 'x':
                            class_object.days_preferred['friday'] = True
                        class_object.section_number = Sections
                        class_object.preferred_time = PreferredTime
                        class_object.preferred_room_type = PreferredRoomType

                        classes.append(class_object)

            except Exception as e:
                print(e)    

    with open('rooms.csv') as csvDataFile:
        csvReader = csv.reader(csvDataFile)
        for row in csvReader:
            try:
                # 110,70,Case
                Room = row[0]
                if Room != 'Room':
                    Capacity = int(row[1])
                    Type = row[2]

                    room_object = ClassRoom()
                    room_object.room = Room
                    room_object.capacity = Capacity
                    room_object.type = Type

                    rooms.append(room_object)



            except Exception as e:
                print(e)   

    return classes, rooms


def generatePopulation(classes, rooms):
    classes, rooms = loadData()
    # class_numpy = numpy.array(classes)
    # room_numpy = numpy.array(rooms)

    # classes.sort(key=lambda x: x.student_count, reverse=True)
    # rooms.sort(key=lambda x: x.capacity, reverse=True)
    
    #### generate populations until a complete population is created

    complete_population = False
    while complete_population is False:
        complete_population = True

        ### randomize arrays to get variations in populations
        random.shuffle(classes)
        random.shuffle(rooms)

        for class_item in classes:
            days = []
            if class_item.days_preferred['monday']:
                days.append('monday')
            if class_item.days_preferred['tuesday']:
                days.append('tuesday')
            if class_item.days_preferred['wednesday']:
                days.append('wednesday')
            if class_item.days_preferred['thursday']:
                days.append('thursday')
            if class_item.days_preferred['friday']:
                days.append('friday')    
            
            if days:
                day = days.pop(0)
            

                #############  hold the first available slot, 
                # but continue to iterate and try and find a preferred classroom type if a preferred is set and better capacity fit
                room_assignment = None
                time_assignment = None

                for room in rooms:
                
                    if room.capacity >= class_item.student_count:
                        available_slot = room.getFreeSlot(day, class_item.class_length, class_item.preferred_time)
                        if available_slot:
                            conflict = False
                            ## if slot is found -- check other class days for clashes
                            if days:
                                for day_check in days:
                                    if room.checkIfOpenSlot(day_check, available_slot['starttime'], 
                                    available_slot['nextstart']) is False:
                                        conflict = True
                            if conflict is False:
                                ### valid time slot found - assign if no assignment made, else see if better match than previous
                                if room_assignment is None:
                                    room_assignment = room
                                    time_assignment = available_slot
                                else:
                                    ### check for over capacity and room preference fit
                                    if room.capacity < room_assignment.capacity or room.type == class_item.preferred_room_type:
                                        room_assignment = room
                                        time_assignment = available_slot
                                    
                
                ## if room assignment found - assign room and go to next class item
                if room_assignment:
                    
                    insert_index = 0
                    for item in room.assigned_time[day]:
                        if time_assignment['starttime'] > item['starttime']:
                            insert_index += 1
                    
                    room_assignment.assigned_time[day].insert(insert_index, {
                        'starttime': time_assignment['starttime'],
                        'endtime': time_assignment['endtime'],
                        'nextstart': time_assignment['nextstart'],
                        'class': class_item.class_name,
                        'section': class_item.section_number
                    })
                    for day_check in days:
                        
                        insert_index = 0
                        for item in room.assigned_time[day_check]:
                            if time_assignment['starttime'] > item['starttime']:
                                insert_index += 1
                        

                        room_assignment.assigned_time[day_check].insert(insert_index, {
                            'starttime': time_assignment['starttime'],
                            'endtime': time_assignment['endtime'],
                            'nextstart': time_assignment['nextstart'],
                            'class': class_item.class_name,
                            'section': class_item.section_number
                        })
                    class_item.assigned_room_time = time_assignment['starttime']
                    class_item.assigned_room_number = room_assignment.room
                    class_item.assigned_room_size = room_assignment.capacity
                    class_item.assigned_room_type = room_assignment.type
                    class_item.getFitness()
                    
                    # print(class_item.class_name, class_item.section_number, room_assignment.room, class_item.assigned_room_time, day, 'student count: ', class_item.student_count, 'capacity: ', room_assignment.capacity)
                else:
                    # print('issue in room assignment - no available time found', class_item.class_name, class_item.section_number, class_item.student_count)
                    complete_population = False  
    for room in rooms:
        room.calculateOpenSlots()
        
        # print(room.room)
        # print('>>>>>> monday', room.assigned_time['monday'])
        # print('>>>>>> tuesday', room.assigned_time['tuesday'])
        # print('>>>>>> wednesday', room.assigned_time['wednesday'])
        # print('>>>>>> thursday', room.assigned_time['thursday'])
        # print('>>>>>> friday', room.assigned_time['friday'])

    # for class_item in classes:
    #     print(
    #         class_item.class_name, 
    #         class_item.section_number, 
    #         class_item.assigned_room_number, 
    #         class_item.assigned_room_time, day, 
    #         'student count: ', 
    #         class_item.student_count, 
    #         'capacity: ', 
    #         class_item.assigned_room_size,
    #         'fitness: ',
    #         class_item.fitnessScore
    #         )
    
    return classes, rooms

    
    
    # print('population generated successfully ')


def geneticAlgorithm():
    ## load data from CSV
    generation_classes, generation_rooms = loadData()
    generation_number = 0
    improvements = []
    improvement_average = 0
    generations = []
    
    ### initial data copy
    # generation_classes = classes.copy()
    # generation_rooms = rooms.copy()

    while improvement_average > 1 or len(improvements) <= 10:
        elite_percent = random.randint(5, 75)
        crossover_percent = random.randint(40, 80)
        mutation_percent = random.randint(5, 20)
        solutions_to_generate = random.randint(200, 1000)


        solution_number = 0
        solutions = []
        while solution_number < solutions_to_generate:
            solution_classes, solution_rooms = generatePopulation(generation_classes.copy(), generation_rooms.copy())
            avg_fitness = round(sum(c.fitnessScore for c in solution_classes) / 93, 0)
            avg_freeslot = round((100 * sum(r.freeTimeBlocks for r in solution_rooms)) / 2880, 0)
            freeRooms = round((100 * sum(r.freeRoom for r in solution_rooms)) / 32, 0)

            ### removed students outside of building because i dont allow it in the generation function
            SolutionFitValue = 0.6 * avg_fitness  +  0.2 * avg_freeslot  +  0.2 * freeRooms
            
            solutions.append({
                'SolutionNumber': solution_number,
                'SolutionFitValue': SolutionFitValue,
                'AvgIndvFitness': avg_fitness,
                'FreeTimeBlocks': avg_freeslot,
                'UnusedRooms': freeRooms,
                'SolutionClasses': solution_classes,
                'SolutionRooms': solution_rooms
            })

            solution_number += 1

        

        ### values accross the entire generation
        avg_fitness = round(sum(c.SolutionFitValue for c in solutions) / solutions_to_generate, 0)
        avg_freeslot = round((100 * sum(r.FreeTimeBlocks for r in solutions)) / solutions_to_generate, 0)
        freeRooms = round((100 * sum(r.UnusedRooms for r in solutions)) / solutions_to_generate, 0)

        SolutionFitValue = 0.6 * avg_fitness  +  0.2 * avg_freeslot  +  0.2 * freeRooms


        generations.append({
            'GenerationNumber': generation_number,
            'solutions': solutions,
            'SolutionFitValue': SolutionFitValue,
            'AvgIndvFitness': avg_fitness,
            'FreeTimeBlocks': avg_freeslot,
            'UnusedRooms': freeRooms,
        })
        if generation_number > 0:
            improvement = SolutionFitValue - generations[generation_number - 1]['SolutionFitValue']
            improvements.append(improvement)
            improvement_average = sum(improvements) / float(len(improvements))
        

        ## sort solutions based on highes overall solution fit value to find elite values   
        ## scrub assigned rooms for non elite values 
        solutions.sort(key=lambda x: x.SolutionFitValue, reverse=True)
        generation_classes = solutions[0]['SolutionClasses']
        generation_rooms = solutions[0]['SolutionRooms']

        generation_classes.sort(key=lambda x: x.fitnessScore, reverse=True)





    








if __name__ == '__main__':
    generatePopulation()
