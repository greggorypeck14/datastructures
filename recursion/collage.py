#!/usr/bin/env python3
import argparse
import os
from PIL import Image               # pip3 install pillow
from foldersearch import find_images
import math


THUMNAILS_PER_ROW = 4
THUMBNAIL_WIDTH = 200
THUMBNAIL_HEIGHT = 200



########################
###  Main program

def main(args):
    '''
    Creates a collage by recursively finding images in a directory path.
    '''
    # find the images
    imgpaths = []
    for filepath in find_images(os.path.abspath(args.searchpath)):
        # print(filepath)
        imgpaths.append(filepath)
    # print(len(imgpaths))
    if len(imgpaths) == 0:
        print('No images found')
        return

    # create a new, RGB image
    rows_needed = math.ceil(len(imgpaths)/4)
    new_collage = Image.new('RGB', (200*4, 200*rows_needed))
    current_column = 0
    current_row = 0
    # print(rows_needed)

    # place the thumbnails
    for imgnum, imgpath in enumerate(imgpaths):
        # print(f'=> {imgpath}')
        # open the image and convert to RGB
        # resize to a thumnail
        # paste in next position

        thumbnail = Image.open(imgpath)
        thumbnail.convert('RGB')
        thumbnail.thumbnail((200, 200))
        if current_column == 4:
            current_column = 0
            current_row += 1
        
        new_collage.paste(thumbnail, (200*current_column, 200*current_row))
        current_column += 1
    # save the image
    # print(f'Writing {args.collage}')

    new_collage.save(args.collage)

    # print(args.collage, args.searchpath)


########################
###  Bootstrap

parser = argparse.ArgumentParser(description='Creates a collage by recursively finding images in a directory path')
parser.add_argument('collage', help='file name to write the collage to')
parser.add_argument('searchpath', help='directory path to start searching')
args = parser.parse_args()
main(args)
