#!/usr/bin/env python3
import os
import fnmatch


IMAGE_GLOBS = {
    '*.png',
    '*.jpg',
}

def is_image(filename):
    '''
    Returns True if the given filename matches one of the IMAGE_GLOBS patterns.
    Just check the filename itself (don't inspect the file contents).
    '''
    # print('find images fired')
    for pattern in IMAGE_GLOBS:
        if fnmatch.fnmatch(filename, pattern):
            return True
    return False
    


def find_images(rootpath, subpath=''):
    '''
    Generator function that returns the images in the given directory
    tree (includes subdirectories). The returned image paths are relative to
    the given path.

    Use os.listdir() to get the files in the current directory (don't use os.walk
    or glob.glob).
    '''
    # print('find images fired')
    # print(rootpath)
    path = os.path.join(rootpath, subpath)
    dirs = os.listdir(path)
    for item in dirs:
        file_path = os.path.join(path, item)
        if os.path.isdir(file_path):
            # print('directory found')
            # print(path, item)
            try:
                yield from find_images(path, item)
            except Exception as e:
                print(e)
        else:
            if is_image(item):
                yield file_path
        
    return find_images


    # pass    # placeholder here so the file will compile
