import csv
from linkedlist_api import LinkedList

def main():
    try:
                
        # output_file = open("output.txt", "w")

        linked_list = None
        output_line = 0

        with open('data.csv') as csvDataFile:
            csvReader = csv.reader(csvDataFile)
            for row in csvReader:
                try:
                    print (str(output_line) + ':' + row[0] + "," + row[1] + "," + row[2])
                    # output_file.write(str(output_line) + ':' + row[0] + "," + row[1] + "," + row[2] +"\n")
                    
                    method = row[0]
                    param1 = row[1]
                    param2 = row[2]
                    output = None

                    if method == 'CREATE':
                        linked_list = LinkedList()
                    if linked_list:
                        if method == 'DEBUG':
                            linked_list.debug_print()
                        elif method == 'ADD':
                            linked_list.add(param1)
                        elif method == 'INSERT':
                            linked_list.insert(param1, param2)
                        elif method == 'SET':
                            linked_list.set(param1, param2)
                        elif method == 'GET':
                            linked_list.get(param1)
                        elif method == 'DELETE':
                            linked_list.delete(param1)
                        elif method == 'SWAP':
                            linked_list.swap(param1, param2)
                   
                    
                except Exception as e:
                    return (str(e))
                    # output_file.write(str(e) +"\n")

                # print(method, param1, param2)
                output_line += 1
        # output_file.close() 


    except Exception as e:
        return(e)

if __name__ == '__main__':
  main()