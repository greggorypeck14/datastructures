#!/usr/bin/env python3


class LinkedList(object):
    '''
    A linked list implementation that holds arbitrary objects.
    '''

    def __init__(self):
        '''Creates a linked list.'''
        self.head = None
        self.size = 0


    def debug_print(self):
        '''Prints a representation of the entire list.'''
        print('{} >>> {}'.format(self.size, ', '.join([ str(item) for item in self ])))
        return '{} >>> {}'.format(self.size, ', '.join([ str(item) for item in self ]))


    def __iter__(self):
        '''Iterates through the linked list, implemented as a generator.'''
        for node in self._iter_nodes():
            yield node.value


    def _iter_nodes(self):
        '''Iterates through the nodes of the list, implemented as a generator.'''
        current = self.head
        while current != None:
            yield current
            current = current.next


    def _get_node(self, index):
        '''Retrieves the Node object at the given index.  Throws an exception if the index is not within the bounds of the linked list.'''
        if index < self.size:
            counter = 0
            for node in self._iter_nodes():
                if counter == index:
                    return node
                counter += 1
            # return self._iter_nodes(index)


        else:
            raise ValueError('Error: ' + str(index) + ' is not within the bounds of the current array.')


    def add(self, item):
        '''Adds an item to the end of the linked list.'''
        if self.size == 0:
            self.head = Node(item)
            self.size += 1
        else:
            try:
                # last_node = next(self._iter_nodes())
                # print(last_node)
                
                last_node = self._get_node(self.size - 1)
                last_node.next = Node(item)
                self.size += 1

                # for node in self._iter_nodes():
                #     print('entered iteration', node.value)
                    # if node.next is None:
                    #     node.next = Node(item)
                    #     self.size += 1
                
                
            except Exception as e:
                print(e)
                return e



    def insert(self, index, item):
        '''Inserts an item at the given index, shifting remaining items right.'''
        index = self.convertIndex(index)
        try:
            previous_node = self._get_node(index - 1)
            current_item = previous_node.next
            new_node = Node(item)
            new_node.next = current_item
            previous_node.next = new_node
            self.size += 1
        except Exception as e:
            print(e)
            return e


    def set(self, index, item):
        '''Sets the given item at the given index.  Throws an exception if the index is not within the bounds of the linked list.'''
        index = self.convertIndex(index)
        try:
            current_node = self._get_node(index)
            current_node.value = item
        except Exception as e:
            print(e)
            return e

    def get(self, index):
        '''Retrieves the item at the given index.  Throws an exception if the index is not within the bounds of the linked list.'''
        index = self.convertIndex(index)
        try:
            node = self._get_node(index)
            print(node.value)
            return node.value
        except Exception as e:
            print(e)
            return e

    def delete(self, index):
        '''Deletes the item at the given index. Throws an exception if the index is not within the bounds of the linked list.'''
        index = self.convertIndex(index)
        try:
            if index > 0:
                previous_node = self._get_node(index - 1)
                current_item = self._get_node(index)
                previous_node.next = current_item.next
                self.size = self.size - 1
            else:
                if self.size > 1:
                    current_item = self._get_node(index)
                    self.head = current_item.next
                    self.size = self.size - 1
                else:
                    raise ValueError('Error: ' + str(index) + ' is not within the bounds of the current array.')

                    
            
        except Exception as e:
            print(e)
            return e

    def swap(self, index1, index2):
        '''Swaps the values at the given indices.'''
        index1 = self.convertIndex(index1)
        index2 = self.convertIndex(index2)
        try:
            item1 = self._get_node(index1)
            item2 = self._get_node(index2)
            temp_value = item1.value
            item1.value = item2.value
            item2.value = temp_value
        except Exception as e:
            print(e)
            return e

    def convertIndex(self, index):
        try:
            index = int(index)
            return index
        except Exception as e:
            raise ValueError('Error: ' + str(index) + ' is not a valid index integer.')



######################################################
###   A node in the linked list

class Node(object):
    '''A node on the linked list'''

    def __init__(self, value):
        self.value = value
        self.next = None

    def __str__(self):
        return '<Node: {}>'.format(self.value)
