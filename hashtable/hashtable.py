#!/usr/bin/env python3
import os, os.path, binascii
from collections import namedtuple
from io import StringIO
import math
from PIL import Image


# a named tuple to hold an individual key and value
# this Node "object" is never seen outside this class
# (e.g. get() returns the value, not the full Node)
Node = namedtuple("Node", ( 'key', 'value' ))

# This is super small because we want to test the loading and print for debugging easier
NUM_BUCKETS = 10


class Hashtable(object):
    '''
    An abstract hashtable superclass.
    '''
    def __init__(self):
        self.buckets = []
        #TODO: initialize the buckets to empty lists
        # self.buckets = [[]] * NUM_BUCKETS
        for i in range(0, NUM_BUCKETS):
            self.buckets.append([])
            

    def set(self, key, value):
        '''
        Adds the given key=value pair to the hashtable.
        '''
        #TODO: store the value by the hash of the key
        bucket_index = self.get_bucket_index(key)
        # print(bucket_index)
        sub_array = self.buckets[bucket_index]
        # print(sub_array)
        sub_array.append(Node(key, value))
        self.buckets[bucket_index] = sub_array

    def get(self, key):
        '''
        Retrieves the value under the given key.
        Returns None if the key does not exist.
        '''
        #TODO: get the value by the hash of the key
        bucket_index = self.get_bucket_index(key)
        sub_array = self.buckets[bucket_index]
        value = None
        for node in sub_array:
            if key == node.key:
                value = node.value
        return value


    def remove(self, key):
        '''
        Removes the given key from the hashtable.
        Returns silently if the key does not exist.
        '''
        #TODO: remove the value by the hash of the key
        bucket_index = self.get_bucket_index(key)
        sub_array = self.buckets[bucket_index]
        # remove_index = None
        for index, node in enumerate(self.buckets[bucket_index]):
            if key == node.key:
                del self.buckets[bucket_index][index]
        #         remove_index = index
        # if remove_index:
        #     del sub_array[remove_index]
        #     self.buckets[bucket_index] = sub_array


    def get_bucket_index(self, key):
        '''
        Returns the bucket index number for the given key.
        The number will be in the range of the number of buckets.
        '''
        # leave this method as is - write your code in the subclasses
        raise NotImplementedError('This method is abstract!  The subclass must define it.')



    ##################################################
    ###   Helper methods

    def __repr__(self):
        '''Returns a representation of the hash table'''
        buf = StringIO()
        for i, bkt in enumerate(self.buckets):
            for j, node in enumerate(bkt):
                buf.write('{:>5}  {}\n'.format(
                    '[{}]'.format(i) if j == 0 else '',
                    node.key,
                ))
        return buf.getvalue()



######################################################
###   String hash table

class StringHashtable(Hashtable):
    '''A hashtable that takes string keys'''

    def get_bucket_index(self, key):
        '''
        Returns the bucket index number for the given key.
        The number will be in the range of the number of buckets.
        '''
        #TODO: hash the string and return the bucket index that should be used
        sub_string = key[0: math.floor(len(key) / 2)]
        numbersum = 0
        for char in sub_string:
            numbersum += ord(char)
        number_average = math.floor(numbersum / len(sub_string))
        return int(math.fmod(number_average, NUM_BUCKETS))




######################################################
###   Guid hash table

COUNTER_CHARS = ( 16, 24 )

class GuidHashtable(Hashtable):
    '''A hashtable that takes GUID keys'''

    def get_bucket_index(self, key):
        '''
        Returns the bucket index number for the given key.
        The number will be in the range of the number of buckets.
        '''
        #TODO: hash the string and return the bucket index that should be used    
        numbersum = 0
        for char in key:
            numbersum += ord(char)
        return int(math.fmod(numbersum, NUM_BUCKETS))



######################################################
###   Image hash table

NUM_CHUNKS = 8

class ImageHashtable(Hashtable):
    '''A hashtable that takes image name keys and creates the hash from (some of) the bytes of the file.'''

    def get_bucket_index(self, key):
        '''
        Returns the bucket index number for the given key.
        The number will be in the range of the number of buckets.
        '''
        #TODO: hash the string and return the bucket index that should be used
        filename = 'images/' + key
        image = Image.open(filename)
        image_list = list(image.getdata())[100:200]
        numbersum = 0
        
        for item in image_list:
            numbersum += item[0]
            numbersum += item[1]
            numbersum += item[2]
            numbersum += item[3]
        numbersum = numbersum / 33
        return int(math.fmod(numbersum, NUM_BUCKETS))
