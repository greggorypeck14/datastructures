#!/usr/bin/env python3

class BaseXConverter(object):
    '''
    Converts positive, base-10 numbers to base-X numbers using a custom alphabet.
    The base is given by the length of the alphabet specified in the constructor.
    The first character in the alphabet has a value of zero,
    the second character a value of one, and so forth.

    Examples:
        Base2:  BaseXConverter('01')
        Base2:  BaseXConverter('<>')     # custom alphabet: < is a zero value and > is a one value
        Base4:  BaseXConverter('0123')
        Base20: BaseXConverter('0123456789abcdefghij')

    See the unit tests at the bottom of the file for many examples.
    '''

    def __init__(self, alphabet):
        '''
        The base is taken from the number of characters in the alphabet.
        '''
        self.alphabet = list(alphabet)
        self.base = len(alphabet)
        self.alphabet_index = {}
        counter = 0
        for char in alphabet:
            self.alphabet_index[counter] = char
            counter += 1

    def convert(self, val):
        '''
        Converts value from base 10 to base X.
        The return value is a baseX integer, wrapped as a string.
        '''
        quotient, remainder = val // self.base, val % self.base
        bXval = str(self.alphabet[remainder])
        
        while quotient > 0:
            new_quotient, remainder = quotient // self.base, quotient % self.base
            bXval = str(self.alphabet_index[remainder]) + bXval
            quotient = new_quotient

        return bXval

    def invert(self, bXval):
        '''
        Converts a value from base X to base 10.
        The bXval should be a baseX integer, wrapped as a string.
        Raises a ValueError if bXval contains any chars not in the alphabet.
        '''
        val = 0
        counter = 0
        reversed_string = bXval[::-1]
        for char in reversed_string:
            try:
                number = list(self.alphabet_index.keys())[list(self.alphabet_index.values()).index(char)]
                val += (number * (self.base ** counter))
                counter += 1
            except Exception as e:
                print(e)
                print('Invalid number found')
                val = 0
                break
        return val
