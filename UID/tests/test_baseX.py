# import sys
# sys.path.append("..")
from unittest import TestCase

from baseX import BaseXConverter
# from .basex import BaseXConverter

# Run this from its parent directory:
#
#    python3 -m unittest tests/test_baseX
#

class BaseXTester(TestCase):

    def test_base2(self):
        conv = BaseXConverter('01')
        i = 1234567890
        self.assertEqual(conv.convert(i), '{:0b}'.format(i))    # test against python's builtin
        self.assertEqual(i, conv.invert(conv.convert(i)))

    def test_base16(self):
        conv = BaseXConverter('0123456789ABCDEF')
        i = 1234567890
        self.assertEqual(i, conv.invert(conv.convert(i)))

    def test_base58(self):
        conv = BaseXConverter('123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz')
        i = 1234567890
        self.assertEqual(i, conv.invert(conv.convert(i)))

    def test_base64(self):
        conv = BaseXConverter('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_')
        i = 1234567890
        self.assertEqual(i, conv.invert(conv.convert(i)))