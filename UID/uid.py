#!/usr/bin/env python3


import time
import base2
# from uid.baseX import BaseXConverter
import baseX
import base16
import base58
import base64


##############################################################################################################################################################################
#  A uid class based on time, counter, and shard id.                                                                                                                         #
#                                                                                                                                                                            #
# |                | Time Component                 | Time Component                            | Space Component                                                            |
# |----------------|--------------------------------|-------------------------------------------|----------------------------------------------------------------------------|
# | Number of Bits | 42 bits                        | 13 bits                                   | 8 bits                                                                     |
# | Description    | Milliseconds since Jan, 1970   | Counter (allows more than one UID per ms) | Shard ID (assigned explicitly to a server, process, or database)           |
# | Maximum Value  | 4,398,046,511,104 (May, 2109)  | 8,192 per ms                              | 256 unique locations                                                       |


# range is 0-255
SHARD_ID = 1

# sizes
MILLIS_BITS = 42
COUNTER_BITS = 13
SHARD_BITS = 8

# the masks
# MILLIS_MASK = 0
COUNTER_MASK = int('1111111111111', 2)
SHARD_MASK = int('11111111', 2)


LAST_MILLIS = 0
COUNTER = 0

MAX_MILLI = int('111111111111111111111111111111111111111111', 2)
MAX_COUNTER = int('1111111111111', 2)


def generate(base=10):
    '''Generates a uid with the given base'''
    # get the millisecond, waiting if needed if we've hit the max counter
    # reset the counter if we are in a new millisecond
    # pack it up and convert base
    global SHARD_ID
    global LAST_MILLIS
    global COUNTER

    current_millisecond = int(round(time.time() * 1000))
    if LAST_MILLIS != current_millisecond:
        LAST_MILLIS = current_millisecond
        COUNTER = 0
    else:
        COUNTER += 1
        if COUNTER > MAX_COUNTER:
            time.sleep(0.001)
            current_millisecond = int(round(time.time() * 1000))
            LAST_MILLIS = current_millisecond
            COUNTER = 0

    # current_millisecond = 1332
    # COUNTER = 17
    # SHARD_ID = 35

    # print(current_millisecond, COUNTER, SHARD_ID)

    uid = pack(current_millisecond, COUNTER, SHARD_ID)
    # print(uid)

    # millis, counter, shard = unpack(uid)
    # print(millis, counter, shard)
    
    # Testing only
    # uid = 12345
    if base == 2:
        uid = base2.convert(uid)
        # print(uid)
        # uid = UID.base2.invert(uid)
    elif base == 16:
        uid = base16.convert(uid)
        # uid = UID.base16.invert(uid)
    elif base == 58:
        uid = base58.convert(uid)
    elif base == 64:
        uid = base64.convert(uid)
    elif base != 10:
        print('invalid base')
        uid = 0
    # print(uid)
    
    return uid


def pack(millis, counter, shard):
    '''Combines the three items into a single uid number'''

    global MILLIS_BITS
    global COUNTER_BITS
    global SHARD_BITS

    milli_bits_needed = MILLIS_BITS - millis.bit_length()
    counter_bits_needed = COUNTER_BITS - counter.bit_length()
    shard_bits_needed = SHARD_BITS - shard.bit_length()

    if milli_bits_needed < 0 or counter_bits_needed < 0 or shard_bits_needed < 0:
        print('too large of number received')
        uid = 0
    else:
        uid = (millis << COUNTER_BITS) | counter
        uid = (uid << SHARD_BITS) | shard
    
    return uid


def unpack(uid):
    '''Separates the uid into its three parts'''

    global COUNTER_BITS
    global SHARD_BITS
    global COUNTER_MASK
    global SHARD_MASK

    millis = uid >> (COUNTER_BITS + SHARD_BITS)

    counter = uid >> SHARD_BITS
    counter = counter & COUNTER_MASK

    shard = uid & SHARD_MASK

    return (millis, counter, shard)

# generate(base=2)
