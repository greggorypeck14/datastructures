
def isAnagram(input):
    input = str.lower(input)
    for counter in range(int(len(input)/2)):
        if input[counter] != input[len(input) - (counter + 1)]:
            return False
    return True
result = isAnagram('racecar')
print(result)