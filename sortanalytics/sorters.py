# Parameters in the following functions:
#   data: a list of tuples
#   index: the tuple index to sort by
#
# Consider the following example data:
#   data = [
#       ( 'homer', 'simpson', 50 ),
#       ( 'luke', 'skywalker', 87 ),
#       ( 'bilbo', 'baggins', 111 ),
#   ]
#
#   bubble_sort(data, 0) sorts on first name (a..z)
#   bubble_sort(data, 0, True) sorts on first name (z..a)
#   bubble_sort(data, 2) sorts on age (1..infinity)
#
# The data list is sorted in place (anew list is not created).
# You do NOT need to perform validation on input data
# (null data list, index out of bounds, etc.)
#

def bubble_sort(data, index, descending=False):
    '''Sorts using the bubble sort algorithm'''
    # data.sort(key=lambda t: t[index], reverse=descending)
    for end_item in range(len(data)-2, -1, -1):
        item_index = 0
        while item_index <= end_item:
            flip = False
            if descending:
                if data[item_index][index] < data[item_index + 1][index]:
                    flip = True
            else:
                if data[item_index][index] > data[item_index + 1][index]:
                    flip = True
            if flip:
                temp = data[item_index]
                data[item_index] = data[item_index + 1]
                data[item_index + 1] = temp
            item_index += 1

def insertion_sort(data, index, descending=False):
    '''Sorts using the insertion sort algorithm'''
    # replace this with your own algorithm (do not use Python's sort)
    # data.sort(key=lambda t: t[index], reverse=descending)
    for item_index in range(0, len(data)):
        if item_index > 0:
            insert_index = item_index
            current_word = data.pop(item_index)
            for sort_index in range(item_index - 1, -1, -1):
                if descending:
                    if current_word[index] > data[sort_index][index]:
                        insert_index = sort_index
                else:
                    if current_word[index] < data[sort_index][index]:
                        insert_index = sort_index
            data.insert(insert_index, current_word) 


def selection_sort(data, index, descending=False):
    '''Sorts using the selection sort algorithm'''
    # replace this with your own algorithm (do not use Python's sort)
    # data.sort(key=lambda t: t[index], reverse=descending)
    for item_index in range(0, len(data)):
        move_item_index = item_index
        for inner_index in range(item_index, len(data)):
            if descending:
                if data[move_item_index][index] < data[inner_index][index]:
                    move_item_index = inner_index
            else:
                if data[move_item_index][index] > data[inner_index][index]:
                    move_item_index = inner_index
        move_item = data.pop(move_item_index)
        data.insert(item_index, move_item)
       

def quick_sort(data, index, descending=False):
    '''Sorts using the quick sort algorithm'''
    # replace this with your own algorithm (do not use Python's sort)
    # data.sort(key=lambda t: t[index], reverse=descending)
    
    # print(data)
  
    quicksortFunction(data, 0, len(data)-1, index, descending)
    # print(data)

def quicksortFunction(data, left_start, right_start, index, desc):
    import math
    pivot_index = math.floor((left_start + right_start)/2)
    pivot_value = data[pivot_index][index]
    initial_index = left_start
    end_index = right_start
    
    while initial_index <= end_index:
        
        if desc:
            while data[initial_index][index] > pivot_value:
                initial_index += 1
            
            while data[end_index][index] < pivot_value:
                end_index -= 1
            
            if initial_index <= end_index:
                temp = data[initial_index]
                data[initial_index] = data[end_index]
                data[end_index] = temp
                initial_index += 1
                end_index -= 1
        else:
            while data[initial_index][index] < pivot_value:
                initial_index += 1
            
            while data[end_index][index] > pivot_value:
                end_index -= 1
            
            if initial_index <= end_index:
                temp = data[initial_index]
                data[initial_index] = data[end_index]
                data[end_index] = temp
                initial_index += 1
                end_index -= 1

    if (left_start < end_index):
        quicksortFunction(data, left_start, end_index, index, desc)

    if (initial_index < right_start):
        quicksortFunction(data, initial_index, right_start, index, desc)


def python_sort(data, index, descending=False):
    '''Sorts using the native Python sort algorithm (Timsort)'''
    # LEAVE this function as is - it will allow you to see your sorts against the python one
    data.sort(key=lambda t: t[index], reverse=descending)




### Main runner ###
if __name__ == '__main__':
  
    data = [95,66,76,77,80,43,26,64,30,39,86,29,16,37,74,5,45,96,55]
    quick_sort(data, 0)
    print(data)
